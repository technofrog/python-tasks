#! /usr/bin/python 

import sys
import math

def main():
    if len(sys.argv) != 4:
        print("Usage: " + sys.argv[0] + " a b c")
        return

    a, b, c = [int(x) for x in sys.argv[1:]]
    delta = b ** 2 - 4 * a * c
    if delta < 0:
        print("Equation has no real solutions")
        return
    elif delta == 0:
        result = (-b) / 2*a
        print("One solution: " + str(result))
    else:
        result1 = (-b - math.sqrt(delta)) / 2*a
        result2 = (-b + math.sqrt(delta)) / 2*a
        print("Two results: " + str(result1) + " and " + str(result2))
        
main()
