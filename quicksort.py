#! /usr/bin/python 

import random

def quicksort(list):
    print("Received list: ", list)
    lh = []
    rh = []
    if len(list) <= 1:
        return list

    partitionPoint = list[random.randint(0,len(list) - 1)]
    print("Partition point: ", partitionPoint)
    # TODO: optimize this code, it's going to be horrendously slow
    lh = [x for x in list if x < partitionPoint]
    mh = [x for x in list if x == partitionPoint]
    rh = [x for x in list if x > partitionPoint]
    
    print("LH:", lh)
    print("RH:", rh)

    lh2 = quicksort(lh)
    rh2 = quicksort(rh)
    
    return lh2 + mh + rh2


def main():
    random.seed()

    randomList = [random.randint(0,100) for x in range(10)]
    print("Generated numbers: "); print(randomList)

    sortedMy  = quicksort(randomList)
    sortedDef = sorted(randomList)
    print("My results", sortedMy)
    print("Def results",sortedDef)

main()
