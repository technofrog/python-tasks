#! /usr/bin/python 

import numpy as np
import random as r

def randList(count, min = -100, max = 100):
    return [r.randint(min, max) for x in range(0,count)]

r.seed()
a = np.array(randList(4))
b = np.array(randList(4))

print("a = ", a)
print("b = ", b)
print(a @ b)
