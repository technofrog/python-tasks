#! /usr/bin/python 

from xml.dom import minidom

doc = minidom.parse("sample.xml")
catalog = doc.getElementsByTagName("catalog")[0]
books = doc.getElementsByTagName("book")
for book in books:
    bookName = book.getElementsByTagName("title")[0]
    genre = book.getElementsByTagName("genre")[0]
    print("Parsing book: " + bookName.firstChild.data + ", genre = " + genre.firstChild.data)
    if genre.firstChild.data == "Computer":
        price = book.getElementsByTagName("price")[0]
        print("Book price before replace: " + price.firstChild.data)

        newPrice = doc.createTextNode("9.99")
        newPriceNode = doc.createElement("price")
        newPriceNode.appendChild(newPrice)

        book.replaceChild(newPriceNode, price)

xmlOut = open("out.xml", "w")
xmlOut.write(doc.toprettyxml())
