#! /usr/bin/python 

import os

def recurListDir(path, nesting = 0):
    dirList = os.listdir(path)
    for file in dirList:
        print ("\t" * nesting + file)
        fullName = path + "/" + file;
        if os.path.isdir(fullName):
            recurListDir(fullName, nesting + 1)

dirPath = input("Enter path to directory which I should list: ")

recurListDir(dirPath)
