#! /usr/bin/python 


inFilePath = input("Enter path to input file: ")
fileR = open(inFilePath, "r")
outFilePath, extension = inFilePath.split(".")
fileW = open(outFilePath + "_DEL." + extension, "w")

string = fileR.read()
bannedWords = ["się", "i", "oraz", "nigdy", "dlaczego"]
for word in bannedWords:
    string = string.replace(" " + word + " ", " ")

fileW.write(string)
