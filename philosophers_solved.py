#! /usr/bin/python 

import threading as thlib
import time

forks = ["golden", "silver", "bronze", "black", "white"]
locks = [thlib.Lock() for x in range(5)]

def philosopher(num):
    lowerFork = num - 1
    upperFork = num

    flipForks = (num == 0)
    for i in range(10):
        if (flipForks):
            locks[upperFork].acquire()
        else:
            locks[lowerFork].acquire()

        print("Philosopher " + str(num) + " locking left fork")
        time.sleep(0.5)
        
        if (flipForks):
            locks[lowerFork].acquire()
        else:
            locks[upperFork].acquire()
        
        print("Philosopher " + str(num) + " Locked two forks: " + forks[num-1] + " and " + forks[num])
        time.sleep(1)
        
        if (flipForks):
            locks[lowerFork].release()
            locks[upperFork].release()
        else:
            locks[upperFork].release()
            locks[lowerFork].release()

        time.sleep(0.5)

if __name__ == "__main__":
    threads = list()
    for i in range(5):
        print("Starting philosopher " + str(i))
        th = thlib.Thread(target=philosopher, args=(i,))
        threads.append(th)
        th.start()

    for th in threads:
        th.join()

