#! /usr/bin/python 


inFilePath = input("Enter path to input file: ")
fileR = open(inFilePath, "r")
outFilePath, extension = inFilePath.split(".")
fileW = open(outFilePath + "_CHG." + extension, "w")

string = fileR.read()
copyString = str()
bannedWords = {"i": "oraz", "oraz": "i", "nigdy": "prawie nigdy", "dlaczego": "czemu" }
for word in string.split(" "):
    if word in bannedWords:
        copyString = copyString + bannedWords[word] + " "
    else:
        copyString = copyString + word + " "
fileW.write(copyString)
