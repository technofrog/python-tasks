#! /usr/bin/python 

import os
from PIL import Image

fileNameJPG = input("Enter name (or absolute path) of file to convert: ")
rawName, extension = fileNameJPG.split(".")
fileNamePNG = rawName + ".png"

try:
    Image.open(fileNameJPG).save(fileNamePNG)
except IOError:
    print("Conversion of file failed!")
