#! /usr/bin/python 

class Complex:
    def __init__(self, real, imag):
        self.r = float(real)
        self.i = float(imag)
    def sum(self, other):
        return Complex(self.r + other.r, self.i + other.i)
    def difference(self, other):
        return Complex(self.r - other.r, self.i - other.i)
    def product(self, other):
        real = self.r * other.r - self.i * other.i
        imag = self.i * other.r + self.r * other.i
        return Complex(real, imag)
    def division(self, other):
        denominator = other.r ** 2 - other.i ** 2
        real = self.r * other.r + self.i * other.i
        imag = self.i * other.r - self.r * other.i
        return Complex (real/denominator, imag/denominator)
    def conjugate(self):
        return Complex(self.r, -self.i)
    def __str__(self):
        return "(" + str(self.r) + "," + str(self.i) + ")"


def parseComplexNumber(inStr):
    outArr = inStr.split(",")
    if len(outArr) != 2:
        raise ValueError("invalid number to be parsed: " + inStr)
    
    realStr = outArr[0].strip()
    if realStr[0] == "(":
        realStr = realStr[1:]

    imagStr = outArr[1].strip()
    if imagStr[-1] == ")":
        imagStr = imagStr[:-1]
    if (imagStr[-1] == "i" or imagStr[-1] == "j"):
        imagStr = imagStr[:-1]

    return Complex(realStr, imagStr)

def unitTestParse():
    testNumbers = ["1,2", "(1,2)", "1,2i", "1,2j", "(1,2i)", "(1, 2j)", "(1.5, 2i)", "(1,2.5i)"];

    for num in testNumbers:
        print(parseComplexNumber(num))

def matchOp(inStr, op):
    out = inStr.split(op)
    if len(out) == 2:
        return True, [parseComplexNumber(x) for x in out]
    else:
        return False, []

def runEquation(inStr):
    # TODO: can this be beautified?
    match, args = matchOp(inStr, "+")
    if match:
        return args[0].sum(args[1])

    match, args = matchOp(inStr, "-")
    if match:
        return args[0].difference(args[1])

    match, args = matchOp(inStr, "*")
    if match:
        return args[0].product(args[1])

    match, args = matchOp(inStr, "/")
    if match:
        return args[0].division(args[1])
    else:
        raise ValueError("invalid argument")

while True:
    inStr = input("Provide equation: ")
    ret = runEquation(inStr)
    print(ret)

