#! /usr/bin/python 

import numpy as np
import random as r

def makeMatrix(dimension):
    a = np.array([r.randint(0, 10) for x in range(0, dimension**2)])
    return a.reshape(dimension, dimension)


r.seed()
a = makeMatrix(128)
b = makeMatrix(128)
print(a + b)

c = makeMatrix(8)
d = makeMatrix(8)
print(c @ d)

e = makeMatrix(4)
print(e, " determinant: ", np.linalg.det(e))
