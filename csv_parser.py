import csv

def printCsv(csvRead):
    for row in csvRead:
        print("Movie " + row[0] + ", rate " + row[1])

def addRecord(data):
    name = input("Enter movie title ")
    grade = input("Enter movie grade ")
    data.append([name, grade])

def deleteRecord(data):
    index = input("Which item to delete? ")
    # TODO: index validation
    del data[int(index)]

def copyToContainer(csvRead):
    return [line for line in csvRead]

def writeCsv(data):
    writeFile = open("movies.csv", mode="w")
    writer = csv.writer(writeFile, delimiter=",")
    for row in data:
        writer.writerow(row)

def main():
    csvFile = open("movies.csv", mode="r")
    csvRead = csv.reader(csvFile, delimiter=",")
    data = copyToContainer(csvRead)
    printCsv(data)
    while True:
        option = input("Type 1 to add record, 2 to delete record, 3 to close program ")
        if option == "1":
            addRecord(data)
        elif option == "2":
            deleteRecord(data)
        elif option == "3":
            break
        else:
            print("Invalid option!")
    csvFile.close()
    writeCsv(data)

main()

