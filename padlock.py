#! /usr/bin/python 
import os

def createFile(fileName):
    while True:
        code = int(input("Enter code to secure the lock (5 digits): "))
        if code > 0 and code < 99999:
            print("Securing lock...")
            file = open(fileName, "w")
            file.write(str(code))
            break
        else:
            print("Invalid code!")

FILE_NAME = "code.txt"

codeFileExists = os.access(FILE_NAME, os.F_OK)
if codeFileExists:
    file = open(FILE_NAME, "r")
    code = file.read(5);
    while True:
        guess = input("Enter code: ")
        if guess == code:
            print("That's correct code, congratulations!")
            file.close()
            break
        else:
            print("Incorrect code!")
else:
    createFile(FILE_NAME)
