#! /usr/bin/python 

import threading as thlib
import time

forks = ["golden", "silver", "bronze", "black", "white"]
locks = [thlib.Lock(), thlib.Lock(), thlib.Lock(), thlib.Lock(), thlib.Lock()]

def philosopher(num):
    for i in range(10):
        with locks[num - 1]:
            with locks[num]:
                print("Philosopher " + str(num) + " Locked two forks: " + forks[num-1] + " and " + forks[num])
                time.sleep(1)

if __name__ == "__main__":
    threads = list()
    for i in range(5):
        print("Starting philosopher " + str(i))
        th = thlib.Thread(target=philosopher, args=(i,))
        threads.append(th)
        th.start()

    for th in threads:
        th.join()

